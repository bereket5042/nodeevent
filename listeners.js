const { getEventManager } = require("./eventManager");
const { SERVICE_NAME } = require("./constants");

function initListeners() {
  const eventManager = getEventManager();
  eventManager.then((channel) => {
    channel
      .assertExchange(SERVICE_NAME, "topic", { durable: true })
      .then(() => {
        return channel.assertQueue("", { exclussive: true }).then((q) => {
          channel.bindQueue(q.queue, SERVICE_NAME, "USER_HAS_REGISTERED");
          return channel.consume(q.queue, userRegisteredHandler, {
            noAck: false,
          });
        });
      });
  });

  eventManager.then((channel) => {
    channel
      .assertExchange(SERVICE_NAME, "topic", { durable: true })
      .then(() => {
        return channel.assertQueue("", { exclussive: true }).then((q) => {
          channel.bindQueue(q.queue, SERVICE_NAME, "ANOTHER_EVENT_NAME");
          return channel.consume(q.queue, anotherEventHandler, {
            noAck: false,
          });
        });
      });
  });
}

function userRegisteredHandler(msg) {
  setTimeout(() => {
    console.log(msg.content.toString());
  }, 5000);
}

function anotherEventHandler(msg) {
  console.log(msg.content.toString());
}

module.exports = {
  initListeners,
};
