let manager = null;

function initEventManager() {
  manager = require("amqplib")
    .connect("amqp://localhost")
    .then((conn) => {
      return conn.createChannel();
    })
    .catch((err) => console.log(err));
}

function getEventManager() {
  if (manager) {
    return manager;
  }
  throw new Error("EventManager have not been initialized");
}

module.exports = {
  initEventManager,
  getEventManager,
};
