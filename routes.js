// import express = require("express");
const { getEventManager } = require("./eventManager");
const { SERVICE_NAME } = require("./constants");

function initRoutes(app) {
  app.post("/event", (req, res, next) => {
    const { name, payload } = req.body;
    if (name && payload) {
      const eventManager = getEventManager();
      eventManager.then((channel) => {
        channel
          .assertExchange(SERVICE_NAME, "topic", { durable: true })
          .then((ok) => {
            return channel.publish(
              SERVICE_NAME,
              "USER_HAS_REGISTERED",
              Buffer.from("something to do from topic publish")
            );
          });
      });
      res.sendStatus(204);
    } else {
      res.sendStatus(400);
    }
  });
}

module.exports = {
  initRoutes,
};
