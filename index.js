const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");
// const { Server } = require("http");
const { initEventManager } = require("./eventManager");
const { initListeners } = require("./listeners");
const { initRoutes } = require("./routes");

let server = null;

function initApplication() {
  const app = express();
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  initRoutes(app);
  return app;
}

function start() {
  // Create eventManager Instance (only one for all  applciation)
  initEventManager();

  // Add Listeners
  initListeners();

  const app = initApplication();
  server = app.listen(process.env.PORT || 3001, () => {
    console.log(`Server started`);
  });
}

start();
